var https = require('https');

module.exports = {
    searchGallery: (query, page, cb) => {
        let options = {
            hostname: 'api.imgur.com',
            path: '/3/gallery/search/time/' + page + '?q=' + query,
            headers: { 'Authorization': 'Client-ID ' + process.env.IMGUR_CLIENT_ID },
            method: 'GET'
        };

        let req = https.request(options, function (res) {
            let body = '';
            //console.log('statusCode:', res.statusCode);
            //console.log('headers:', res.headers);

            res.on("data", data => {
                body += data;
            });
            res.on("end", () => {
                body = JSON.parse(body);
                cb(null, body);
            });
        });

        req.on('error', function (e) {
            cb(e);
        });

        req.end();
    }
};