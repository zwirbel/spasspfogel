const http = require('http');

const Search = require("./search.js");
const TelegramBot = require('node-telegram-bot-api');
const Imgur = require('./imgur.js');
const ChuckNorris = require('./chucknorris.js');

const token = process.env.TELEGRAM_API_TOKEN;
const bot = new TelegramBot(token, { polling: true });
const searchCfg = process.env.SEARCH_TERMS;



let handlers = {};
let searchTerms = [
    { alias: 'engage!', search: 'star%20trek' }
];

if(searchCfg) {
    console.log('use search terms from env');
    searchTerms = JSON.parse(searchCfg.trim());
}


searchTerms.forEach(element => {
    handlers[element.alias] = Search.createHandler(element.search);
});

// Matches "/random [whatever]"
// TODO cleanup
bot.onText(/\/(.+) (.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    const term = match[1];
    const resp = match[2];
    if(term === 'random' && resp === 'chucknorris') {
        ChuckNorris.getRandomJoke((err, pl) => {
            if (!err && pl.value) {
                bot.sendMessage(chatId, pl.value);
            }
        });
    } else if (term === 'random' && resp) {
        Imgur.searchGallery(require('querystring').escape(resp), 1, (err, pl) => {
            if (!err && pl.data) {
                let cnt = pl.data.length;
                let post = pl.data[Search.getRandomInt(cnt)];
                console.log(msg);
                bot.sendMessage(chatId, post ? post.link : `I'm sorry ${msg.chat.first_name}, I'm afraid I can't do that.`);
            }
        });
    }
});

// Listen for any kind of message
bot.on('message', (msg) => {
    const chatId = msg.chat.id;
    let hdl = handlers[msg.text.trim().toLowerCase()];
    if (hdl) {
        hdl.currentPage = hdl.currentPage + 1;
        hdl.handle(hdl.currentPage, (err, pl) => {
            if (err) {
                console.error(e);
                return;
            }
            let cnt = pl.data.length;
            let idx = Search.getRandomInt(cnt);
            let data = pl.data[idx];
            if (data) {
                bot.sendMessage(chatId, `${hdl.currentPage}/${idx}/${cnt}: ${data.link}`);
                return;
            }
            bot.sendMessage(chatId, `I'm sorry ${msg.chat.first_name}, I'm afraid I can't do that.`);
            hdl.currentPage = 0;
        })
    }
});

const requestListener = function (req, res) {
    res.writeHead(200);
    res.end('spasspfogel is up');
}

const server = http.createServer(requestListener);
server.listen(9090);