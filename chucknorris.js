var https = require('https');

module.exports = {
    getRandomJoke: (cb) => {
        let options = {
            hostname: 'api.chucknorris.io',
            path: '/jokes/random',
            method: 'GET'
        };

        let req = https.request(options, function (res) {
            let body = '';
            res.on("data", data => {
                body += data;
            });
            res.on("end", () => {
                body = JSON.parse(body);
                cb(null, body);
            });
        });

        req.on('error', function (e) {
            cb(e);
        });

        req.end();
    }
};