const Imgur = require('./imgur.js');
module.exports = {
    getRandomInt : (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    },
    createHandler: (searchTerm) => {
        return {
            currentPage: 0,
            handle: (page, thenDo) => {
                Imgur.searchGallery(searchTerm, page, thenDo);
            }
        };
    }
};